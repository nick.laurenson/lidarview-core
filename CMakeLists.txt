add_subdirectory("ApplicationComponents/")
add_subdirectory("LidarPlugin/")
add_subdirectory("Python")
# Add subdirectory containing python script for animation
add_subdirectory("Utilities/Animation")
